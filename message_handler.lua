local MessageHandler = {}

local json = require("cjson")

local Call = require("call")
local CallError = require("callerror")
local CallResult = require("callresult")

MessageHandler.new = function()
    local self = {}

    local handlers = {}
    local callback_map = {}

    local try_send = function(message)
        assert(send_fn, "Trying to send message before setting send function")
        send_fn(message)
    end

    local process_response = function(message)
        local cb = callback_map[message[2]]
        if cb == nil then
            return
        end

        if message[Call.MESSAGE_TYPE_INDEX] == CallError.MESSAGE_TYPE_ID then
            cb({
                    errorCode = message[CallError.MESSAGE_ERROR_CODE_INDEX],
                    errorDescription = message[CallError.MESSAGE_ERROR_DESCRIPTION_INDEX],
                    errorDetails = message[CallError.MESSAGE_ERROR_DETAILS_INDEX]
            }, nil)
        else
            cb(nil, message)
        end
    end

    local process_request = function(message)
        if handlers[message[Call.MESSAGE_ACTION_INDEX]] ~= nil then
           handlers[message[Call.MESSAGE_ACTION_INDEX]](message)
        else
            local not_supported = CallResult.new(message[Call.MESSAGE_ID_INDEX],
                                                 CallResult.ErrorCode.NOT_SUPPORTED)
            try_send(result.to_json())
        end
    end

   self.set_send_fn = function(fn)
        send_fn = fn
    end

    self.send = function(message, timeout, callback)
        print(message.get_id())
        callback_map[message.get_id()] = callback
        try_send(message.to_json())
    end

    self.receive = function(message)
        if message == nil then
            return
        end

        local decoded = json.decode(message)
        if decoded == nil then
            return
        end

        if decoded[1] == Call.MESSAGE_TYPE_ID then
            return process_request(decoded)
        elseif (decoded[1] == CallResult.MESSAGE_TYPE_ID) or
               (decoded[1] == CallError.MESSAGE_TYPE_ID) then
            return process_response(decoded)
        end
    end

    self.add_handler = function(action, handler)
    end

    return self
end

return MessageHandler
