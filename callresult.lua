local CallResult = {}
local uuid = require("uuid")
local json = require("cjson")

CallResult.MESSAGE_TYPE_ID = 3
CallResult.MESSAGE_TYPE_INDEX = 1
CallResult.MESSAGE_ID_INDEX = 2
CallResult.MESSAGE_PAYLOAD_INDEX = 3

CallResult.new = function(id, action, payload)
    local self = {}

    local message = {
        CallResult.MESSAGE_TYPE_ID,
        id,
        action,
        payload
    }

    self.get_payload = function()
        return message.payload
    end

    self.set_payload = function(payload)
        message.payload = payload
    end

    self.get_action = function()
        return message.action
    end

    self.set_action = function(action)
        message.action = action
    end

    self.to_json = function()
        return json.encode(message)
    end

    return self
end

CallResult.from_json = function(json)
    local message = json.decode(json)

    if message == nil then
        return nil
    end

    return CallResult.new(message[Call.MESSAGE_ID_INDEX],
                          message[Call.MESSAGE_ACTION_INDEX],
                          message[Call.MESSAGE_PAYLOAD_INDEX])

end

return CallResult
