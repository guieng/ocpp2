local CallError = {}
local uuid = require("uuid")
local json = require("cjson")

CallError.MESSAGE_TYPE_ID = 4
CallError.MESSAGE_TYPE_INDEX = 1
CallError.MESSAGE_ID_INDEX = 2
CallError.MESSAGE_ERROR_CODE_INDEX = 3
CallError.MESSAGE_ERROR_DESCRIPTION_INDEX = 4
CallError.MESSAGE_ERROR_DETAILS_INDEX = 5

CallError.ErrorCode = {
    FORMAT_VIOLATION = "FormateViolation",
    GENERIC_ERROR = "GenericError",
    INTERNAL_ERROR = "InternalError",
    MESSAGE_TYPE_NOT_SUPPORTED = "MessageTypeNotSupported",
    NOT_IMPLEMENTED = "NotImplemented",
    NOT_SUPPORTED = "NotSupported",
    OCCURRENCE_CONSTRAINT_VIOLATION = "OccurrenceConstraintViolation",
    PROPERTY_CONSTRAINT_VIOLATION = "PropertyConstraintViolation",
    PROTOCOL_ERROR = "ProtocolError",
    RPC_FRAMEWORK_ERROR = "RpcFrameworkError",
    SECURITY_ERROR = "SecurityError",
    TYPE_CONSTRAINT_VIOLATION = "TypeConstraintViolation"
}

CallError.new = function(id, code, description, details)
    local self = {}

    assert(id ~= nil, "MessageId is required")
    assert(code ~= nil, "ErrorCode is required")

    local message = {
        CallError.MESSAGE_TYPE_ID,
        id,
        code,
        description or "",
        details or {},
   }

    self.get_payload = function()
        return message.payload
    end

    self.set_payload = function(payload)
        message.payload = payload
    end

    self.get_action = function()
        return message.action
    end

    self.set_action = function(action)
        message.action = action
    end

    self.to_json = function()
        return json.encode(message)
    end

    return self
end

return CallError
