local StatusNotificationRequest = {}

local json = require("cjson")
local luatz = require("luatz")

StatusNotificationRequest.new = function(payload)
    local self = {}

    local _action = "StatusNotification"
    local _payload = {
        timestamp = payload.timestamp or
                    luatz.timetable.new_from_timestamp(luatz.time()):rfc_3339(),
        connectorStatus = payload.connectorStatus or nil,
        evseId = payload.evseId or nil,
        connectorId = payload.connectorId or nil,
    }

    self.set_timestamp = function(t)
        _payload.timestamp = t
    end

    self.get_timestamp = function()
        return _payload.timestamp
    end

    self.set_connector_status = function(s)
        _payload.connectorStatus = s
    end

    self.get_connector_status = function()
        return _payload.connectorStatus
    end

    self.set_evse_id = function(id)
        _payload.evseId = id
    end

    self.get_evse_id = function()
        return _payload.evseId
    end

    self.set_connector_id = function(id)
        _payload.connectorId = id
    end

    self.get_connector_id = function()
        return _payload.connectorId
    end

    self.get_action = function()
        return _action
    end

    self.get_payload = function()
        return _payload
    end

    return self
end

return StatusNotificationRequest
