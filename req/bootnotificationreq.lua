local BootNotificationRequest = {}
local json = require("cjson")

BootNotificationRequest.new = function(payload)
    local self = {}

    local _action = "BootNotification"
    local _payload = {
        reason = payload.reason or BootReasonEnumType.POWER_UP,
        chargingStation = payload.chargingStation or {}
    }

    self.set_reason = function(r)
        _payload.reason = r
    end

    self.get_reason = function()
        return _payload.reason
    end

    self.set_charging_station = function(cs)
        _payload.chargingStation = cs
    end

    self.get_charging_station = function()
        return _payload.chargingStation
    end

    self.get_action = function()
        return _action
    end

    self.get_payload = function()
        return _payload
    end

    return self
end

return BootNotificationRequest
