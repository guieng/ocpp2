local Call = {}
local json = require("cjson")
local uuid = require("uuid")

Call.MESSAGE_TYPE_ID = 2
Call.MESSAGE_TYPE_INDEX = 1
Call.MESSAGE_ID_INDEX = 2
Call.MESSAGE_ACTION_INDEX = 3
Call.MESSAGE_PAYLOAD_INDEX = 4

uuid.randomseed(os.time() + (os.clock() * 100000))

Call.new = function(action, payload, id)
    local self = {}

    local message = {
        Call.MESSAGE_TYPE_ID,
        id or uuid(),
        action,
        payload
    }

    self.get_id = function()
        return message[Call.MESSAGE_ID_INDEX]
    end

    self.get_payload = function()
        return message[Call.MESSAGE_PAYLOAD.INDEX]
    end

    self.set_payload = function(payload)
        message[Call.MESSAGE_PAYLOAD_INDEX] = payload
    end

    self.get_action = function()
        return message[Call.MESSAGE_ACTION_INDEX]
    end

    self.set_action = function(action)
        message[Call.MESSAGE_ACTION_INDEX] = action
    end

    self.to_json = function()
        return json.encode(message)
    end

    return self
end

Call.from_json = function(json)
    local message = json.decode(json)

    if message == nil then
        return nil
    end

    return Call.new(message[Call.MESSAGE_ACTION_INDEX],
                    message[Call.MESSAGE_PAYLOAD_INDEX],
                    message[Call.MESSAGE_ID_INDEX])
end

return Call
