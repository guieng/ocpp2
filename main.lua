package.path = package.path ..
               ";models/?.lua" ..
               ";req/?.lua" ..
               ";res/?.lua"
require("enumerations")

_G.TURBO_SSL = true

local turbo = require("turbo")
local ws = turbo.websocket

local ChargePoint = require("chargepoint")
local BootNotificationRequest = require("bootnotificationreq")
local StatusNotificationRequest = require("statusnotificationreq")
local MessageHandler = require("message_handler")

local cs = ChargePoint.new()

local bn_message = cs.call(
    BootNotificationRequest.new({
            reason = BootReasonEnumType.POWER_UP,
            chargingStation = {
                model = "G3",
                vendorName = "EVBox"
            }
    })
)

local sn_message = cs.call(
    StatusNotificationRequest.new({
            evseId = 1,
            connectorId = 1,
            connectorStatus = ConnectorStatusEnumType.AVAILABLE,
    })
)


local mh = MessageHandler.new()

local on_open = function(ws)
    local send_fn = function(message)
        ws:write_message(message)
    end
    mh.set_send_fn(send_fn)
    mh.send(bn.message, nil, function(err, resp)
        if err ~= nil then
            print(err.errorCode)
        else
            print(inspect(resp))
        end
    end)
end

local on_message = function(ws, msg)
    mh.receive(msg)
end

turbo.ioloop.instance():add_callback(function()
    ws.WebSocketClient("wss://api.local.everon.io:8443/ocpp20/3/23192A4A-CFE3-4D85-9374-59D3F5805233", {

        on_connect = on_open,
        on_close = function(self)
            print("ws closed")
        end,
        on_error = function(self, code, reason)
            print("error")
        end,
        ssl_options = {
            priv_file = "/home/guilherme/workspace/everon_test/SSL/api.local.everon.io/station.key.pem",
            cert_file = "/home/guilherme/workspace/everon_test/SSL/api.local.everon.io/ocpp.pem",
            ca_path = "/home/guilherme/workspace/everon_test/SSL/api.local.everon.io/subCA.pem",
            verify_ca = true,
        }
    })
end):start()
