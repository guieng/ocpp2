local cjson = require("cjson")
local jsonschema = require("jsonschema")
local inspect = require("inspect")

local f = assert(io.open("schemas/TransactionEventRequest_v1p0.json"))

sch = cjson.decode(assert(f:read("*a")))

local validator = jsonschema.generate_validator(sch)

transaction = [[
[3, "123", "TransactionEvent",
{
	"timestamp": "2019-10-14T15:05:38.0Z",
	"triggerReason": "ChargingStateChanged",
	"seqNo": 2,
	"evse": {
		"id": 1,
		"connectorId": 1
	},
	"transactionData": {
		"id": "269EF724-FDE4-4F09-BC65-15545A4BF6CB",
		"chargingState": "Charging"
	},
	"idToken": {
		"idToken": "04D645624B4D80",
		"type": "ISO14443"
	},
	"offline": false
}]
]]

t = cjson.decode(transaction)

print(inspect(t[4]))
print(validator(t[4]))
