local ChargePoint = {}
local Call = require("call")

ChargePoint.new = function(station_id)
    local self = {}

    local private = {
        evse_list = {},
        id = station_id,
    }

    self.call = function(req)
        return Call.new(req.get_action(), req.get_payload())
    end

    return self

end

return ChargePoint
