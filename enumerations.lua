AttributeEnumType = {
	ACTUAL = "Actual",
	TARGET = "Target",
	MIN_SET = "MinSet",
	MAX_SET = "MaxSet",
}

ConnectorStatusEnumType = {
    AVAILABLE = "Available",
    OCCUPIED = "Occupied",
    RESERVED = "Reserved",
    UNAVAILABLE = "Unavailable",
    FAULTED = "Faulted",
}

DataEnumType = {
	STRING = "string",
	DECIMAL = "decimal",
	INTEGER = "integer",
	DATE_TIME = "dateTime",
	BOOLEAN = "boolean",
	OPTION_LIST = "OptionList",
	SEQUENCE_LIST = "SequenceList",
	MEMBER_LIST = "MemberList",
}

MutabilityEnumType = {
	READ_ONLY = "ReadOnly",
	WRITE_ONLY = "WriteOnly",
	READ_WRITE = "ReadWrite",
}

MonitorEnumType = {
	UPPER_THRESHOLD = "UpperThreshold",
	LOWER_THRESHOLD = "LowerThreshold",
	DELTA = "Delta",
	PERIODIC = "Periodic",
	PERIODIC_CLOCK_ALIGNED = "PeriodicClockAligned",
}

AuthorizationStatusEnumType = {
	ACCEPTED = "Accepted",
	BLOCKED = "Blocked",
	CONCURRENT_TX = "ConcurrentTx",
	EXPIRED = "Expired",
	INVALID = "Invalid",
	NO_CREDIT = "NoCredit",
	NOT_ALLOWED_TYPE_EVSE = "NotAllowedTypeEVSE",
	NOT_AT_THIS_LOCATION = "NotAtThisLocation",
	NOT_AT_THIS_TIME = "NotAtThisTime",
	UNKNOWN = "Unknown",
}

IdTokenEnumType = {
	CENTRAL = "Central",
	E_MAID = "eMAID",
	ISO14443 = "ISO14443",
	KEY_CODE = "KeyCode",
	LOCAL = "Local",
	NO_AUTHORIZATION = "NoAuthorization",
	ISO15693 = "ISO15693",
}

MessageFormatEnumType = {
	ASCII = "ASCII",
	HTML = "HTML",
	URI = "URI",
	UTF8 = "UTF8",
}

ChargingLimitSourceEnumType = {
	EMS = "EMS",
	OTHER = "Other",
	SO = "SO",
	CSO = "CSO",
}

ChargingRateUnitEnumType = {
	W = "W",
	A = "A",
}

ChargingProfilePurposeEnumType = {
	CHARGING_STATION_EXTERNAL_CONSTRAINTS = "ChargingStationExternalConstraints",
	CHARGING_STATION_MAX_PROFILE = "ChargingStationMaxProfile",
	TX_DEFAULT_PROFILE = "TxDefaultProfile",
	TX_PROFILE = "TxProfile",
}

EnergyTransferModeEnumType = {
	AC_SINGLE_PHASE_CORE = "AC_single_phase_core",
	AC_THREE_PHASE_CORE = "AC_three_phase_core",
	DC_COMBO_CORE = "DC_combo_core",
	DC_CORE = "DC_core",
	DC_EXTENDED = "DC_extended",
	DC_UNIQUE = "DC_unique",
}

MessageTriggerEnumType = {
	BOOT_NOTIFICATION = "BootNotification",
	LOG_STATUS_NOTIFICATION = "LogStatusNotification",
	FIRMWARE_STATUS_NOTIFICATION = "FirmwareStatusNotification",
	HEARTBEAT = "Heartbeat",
	METER_VALUES = "MeterValues",
	SIGN_CHARGING_STATION_CERTIFICATE = "SignChargingStationCertificate",
	SIGN_V2_G_CERTIFICATE = "SignV2GCertificate",
	STATUS_NOTIFICATION = "StatusNotification",
	TRANSACTION_EVENT = "TransactionEvent",
}

GetVariableStatusEnumType = {
	ACCEPTED = "Accepted",
	REJECTED = "Rejected",
	UNKNOWN_COMPONENT = "UnknownComponent",
	UNKNOWN_VARIABLE = "UnknownVariable",
	NOT_SUPPORTED_ATTRIBUTE_TYPE = "NotSupportedAttributeType",
}

HashAlgorithmEnumType = {
	SHA256 = "SHA256",
	SHA384 = "SHA384",
	SHA512 = "SHA512",
}

EventTriggerEnumType = {
	ALERTING = "Alerting",
	DELTA = "Delta",
	PERIODIC = "Periodic",
}

ChargingStateEnumType = {
	CHARGING = "Charging",
	EV_DETECTED = "EVDetected",
	SUSPENDED_EV = "SuspendedEV",
	SUSPENDED_EVSE = "SuspendedEVSE",
}

EncodingMethodEnumType = {
	OTHER = "Other",
	DLMS_MESSAGE = "DLMSMessage",
	COSEM_PROTECTED_DATA = "COSEMProtectedData",
	EDL = "EDL",
}

LocationEnumType = {
	BODY = "Body",
	CABLE = "Cable",
	EV = "EV",
	INLET = "Inlet",
	OUTLET = "Outlet",
}

MeasurandEnumType = {
	CURRENT__EXPORT = "Current_Export",
	CURRENT__IMPORT = "Current_Import",
	CURRENT__OFFERED = "Current_Offered",
	ENERGY__ACTIVE__EXPORT__REGISTER = "Energy_Active_Export_Register",
	ENERGY__ACTIVE__IMPORT__REGISTER = "Energy_Active_Import_Register",
	ENERGY__REACTIVE__EXPORT__REGISTER = "Energy_Reactive_Export_Register",
	ENERGY__REACTIVE__IMPORT__REGISTER = "Energy_Reactive_Import_Register",
	ENERGY__ACTIVE__EXPORT__INTERVAL = "Energy_Active_Export_Interval",
	ENERGY__ACTIVE__IMPORT__INTERVAL = "Energy_Active_Import_Interval",
	ENERGY__ACTIVE__NET = "Energy_Active_Net",
	ENERGY__REACTIVE__EXPORT__INTERVAL = "Energy_Reactive_Export_Interval",
	ENERGY__REACTIVE__IMPORT__INTERVAL = "Energy_Reactive_Import_Interval",
	ENERGY__REACTIVE__NET = "Energy_Reactive_Net",
	ENERGY__APPARENT__NET = "Energy_Apparent_Net",
	ENERGY__APPARENT__IMPORT = "Energy_Apparent_Import",
	ENERGY__APPARENT__EXPORT = "Energy_Apparent_Export",
	FREQUENCY = "Frequency",
	POWER__ACTIVE__EXPORT = "Power_Active_Export",
	POWER__ACTIVE__IMPORT = "Power_Active_Import",
	POWER__FACTOR = "Power_Factor",
	POWER__OFFERED = "Power_Offered",
	POWER__REACTIVE__EXPORT = "Power_Reactive_Export",
	POWER__REACTIVE__IMPORT = "Power_Reactive_Import",
	SO_C = "SoC",
	VOLTAGE = "Voltage",
}

PhaseEnumType = {
	L1 = "L1",
	L2 = "L2",
	L3 = "L3",
	N = "N",
	L1_N = "L1_N",
	L2_N = "L2_N",
	L3_N = "L3_N",
	L1_L2 = "L1_L2",
	L2_L3 = "L2_L3",
	L3_L1 = "L3_L1",
}

ReadingContextEnumType = {
	INTERRUPTION__BEGIN = "Interruption_Begin",
	INTERRUPTION__END = "Interruption_End",
	OTHER = "Other",
	SAMPLE__CLOCK = "Sample_Clock",
	SAMPLE__PERIODIC = "Sample_Periodic",
	TRANSACTION__BEGIN = "Transaction_Begin",
	TRANSACTION__END = "Transaction_End",
	TRIGGER = "Trigger",
}

ReasonEnumType = {
	DE_AUTHORIZED = "DeAuthorized",
	EMERGENCY_STOP = "EmergencyStop",
	ENERGY_LIMIT_REACHED = "EnergyLimitReached",
	EV_DISCONNECTED = "EVDisconnected",
	GROUND_FAULT = "GroundFault",
	IMMEDIATE_RESET = "ImmediateReset",
	LOCAL = "Local",
	LOCAL_OUT_OF_CREDIT = "LocalOutOfCredit",
	MASTER_PASS = "MasterPass",
	OTHER = "Other",
	OVERCURRENT_FAULT = "OvercurrentFault",
	POWER_LOSS = "PowerLoss",
	POWER_QUALITY = "PowerQuality",
	REBOOT = "Reboot",
	REMOTE = "Remote",
	SOC_LIMIT_REACHED = "SOCLimitReached",
	STOPPED_BY_EV = "StoppedByEV",
	TIME_LIMIT_REACHED = "TimeLimitReached",
	TIMEOUT = "Timeout",
	UNLOCK_COMMAND = "UnlockCommand",
}

SignatureMethodEnumType = {
	ECDSAP256_SHA256 = "ECDSAP256SHA256",
	ECDSAP384_SHA384 = "ECDSAP384SHA384",
	ECDSA192_SHA256 = "ECDSA192SHA256",
}

TransactionEventEnumType = {
	ENDED = "Ended",
	STARTED = "Started",
	UPDATED = "Updated",
}

TriggerReasonEnumType = {
	AUTHORIZED = "Authorized",
	CABLE_PLUGGED_IN = "CablePluggedIn",
	CHARGING_RATE_CHANGED = "ChargingRateChanged",
	CHARGING_STATE_CHANGED = "ChargingStateChanged",
	DEAUTHORIZED = "Deauthorized",
	ENERGY_LIMIT_REACHED = "EnergyLimitReached",
	EV_COMMUNICATION_LOST = "EVCommunicationLost",
	EV_CONNECT_TIMEOUT = "EVConnectTimeout",
	METER_VALUE_CLOCK = "MeterValueClock",
	METER_VALUE_PERIODIC = "MeterValuePeriodic",
	TIME_LIMIT_REACHED = "TimeLimitReached",
	TRIGGER = "Trigger",
	UNLOCK_COMMAND = "UnlockCommand",
	STOP_AUTHORIZED = "StopAuthorized",
	EV_DEPARTED = "EVDeparted",
	EV_DETECTED = "EVDetected",
	REMOTE_STOP = "RemoteStop",
	REMOTE_START = "RemoteStart",
}

APNAuthenticationEnumType = {
	CHAP = "CHAP",
	NONE = "NONE",
	PAP = "PAP",
	AUTO = "AUTO",
}

OCPPInterfaceEnumType = {
	WIRED0 = "Wired0",
	WIRED1 = "Wired1",
	WIRED2 = "Wired2",
	WIRED3 = "Wired3",
	WIRELESS0 = "Wireless0",
	WIRELESS1 = "Wireless1",
	WIRELESS2 = "Wireless2",
	WIRELESS3 = "Wireless3",
}

OCPPTransportEnumType = {
	JSON = "JSON",
	SOAP = "SOAP",
}

OCPPVersionEnumType = {
	OCPP12 = "OCPP12",
	OCPP15 = "OCPP15",
	OCPP16 = "OCPP16",
	OCPP20 = "OCPP20",
}

VPNEnumType = {
	IK_EV2 = "IKEv2",
	IP_SEC = "IPSec",
	L2_TP = "L2TP",
	PPTP = "PPTP",
}

LogEnumType = {
	DIAGNOSTICS_LOG = "DiagnosticsLog",
	SECURITY_LOG = "SecurityLog",
}

ChargingProfileKindEnumType = {
	ABSOLUTE = "Absolute",
	RECURRING = "Recurring",
	RELATIVE = "Relative",
}

RecurrencyKindEnumType = {
	DAILY = "Daily",
	WEEKLY = "Weekly",
}

GetInstalledCertificateStatusEnumType = {
	ACCEPTED = "Accepted",
	NOT_FOUND = "NotFound",
}

MessagePriorityEnumType = {
	ALWAYS_FRONT = "AlwaysFront",
	IN_FRONT = "InFront",
	NORMAL_CYCLE = "NormalCycle",
}

MessageStateEnumType = {
	CHARGING = "Charging",
	FAULTED = "Faulted",
	IDLE = "Idle",
	UNAVAILABLE = "Unavailable",
}

ConnectorEnumType = {
	C_CCS1 = "cCCS1",
	C_CCS2 = "cCCS2",
	C_G105 = "cG105",
	C_TESLA = "cTesla",
	C_TYPE1 = "cType1",
	C_TYPE2 = "cType2",
	S309_1_P_16_A = "s309_1P_16A",
	S309_1_P_32_A = "s309_1P_32A",
	S309_3_P_16_A = "s309_3P_16A",
	S309_3_P_32_A = "s309_3P_32A",
	S_BS1361 = "sBS1361",
	S_CEE_7_7 = "sCEE_7_7",
	S_TYPE2 = "sType2",
	S_TYPE3 = "sType3",
	OTHER1_PH_MAX16_A = "Other1PhMax16A",
	OTHER1_PH_OVER16_A = "Other1PhOver16A",
	OTHER3_PH = "Other3Ph",
	PAN = "Pan",
	W_INDUCTIVE = "wInductive",
	W_RESONANT = "wResonant",
	UNDETERMINED = "Undetermined",
	UNKNOWN = "Unknown",
}

ClearMonitoringStatusEnumType = {
	ACCEPTED = "Accepted",
	REJECTED = "Rejected",
	NOT_FOUND = "NotFound",
}

SetVariableStatusEnumType = {
	ACCEPTED = "Accepted",
	REJECTED = "Rejected",
	INVALID_VALUE = "InvalidValue",
	UNKNOWN_COMPONENT = "UnknownComponent",
	UNKNOWN_VARIABLE = "UnknownVariable",
	NOT_SUPPORTED_ATTRIBUTE_TYPE = "NotSupportedAttributeType",
	OUT_OF_RANGE = "OutOfRange",
	REBOOT_REQUIRED = "RebootRequired",
}

GetCompositeScheduleStatusEnumType = {
	ACCEPTED = "Accepted",
	REJECTED = "Rejected",
}

SetMonitoringStatusEnumType = {
	ACCEPTED = "Accepted",
	UNKNOWN_COMPONENT = "UnknownComponent",
	UNKNOWN_VARIABLE = "UnknownVariable",
	UNSUPPORTED_MONITOR_TYPE = "UnsupportedMonitorType",
	REJECTED = "Rejected",
	OUT_OF_RANGE = "OutOfRange",
	DUPLICATE = "Duplicate",
}

MonitoringCriterionEnumType = {
	THRESHOLD_MONITORING = "ThresholdMonitoring",
	DELTA_MONITORING = "DeltaMonitoring",
	PERIODIC_MONITORING = "PeriodicMonitoring",
}

UpdateEnumType = {
	DIFFERENTIAL = "Differential",
	FULL = "Full",
}

CostKindEnumType = {
	CARBON_DIOXIDE_EMISSION = "CarbonDioxideEmission",
	RELATIVE_PRICE_PERCENTAGE = "RelativePricePercentage",
	RENEWABLE_GENERATION_PERCENTAGE = "RenewableGenerationPercentage",
}

ISO15118EVCertificateStatusEnumType = {
	ACCEPTED = "Accepted",
	FAILED = "Failed",
}

BootReasonEnumType = {
	APPLICATION_RESET = "ApplicationReset",
	FIRMWARE_UPDATE = "FirmwareUpdate",
	LOCAL_RESET = "LocalReset",
	POWER_UP = "PowerUp",
	REMOTE_RESET = "RemoteReset",
	SCHEDULED_RESET = "ScheduledReset",
	TRIGGERED = "Triggered",
	UNKNOWN = "Unknown",
	WATCHDOG = "Watchdog",
}

ComponentCriterionEnumType = {
	ACTIVE = "Active",
	AVAILABLE = "Available",
	ENABLED = "Enabled",
	PROBLEM = "Problem",
}

CertificateStatusEnumType = {
	ACCEPTED = "Accepted",
	SIGNATURE_ERROR = "SignatureError",
	CERTIFICATE_EXPIRED = "CertificateExpired",
	CERTIFICATE_REVOKED = "CertificateRevoked",
	NO_CERTIFICATE_AVAILABLE = "NoCertificateAvailable",
	CERT_CHAIN_ERROR = "CertChainError",
	CONTRACT_CANCELLED = "ContractCancelled",
}
